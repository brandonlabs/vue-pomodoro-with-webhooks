import Vue from 'vue'
import Pomodoro from '@/components/Pomodoro'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

describe('Pomodoro.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Pomodoro)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.pomodoro h1').textContent)
      .to.equal("Let's focus")
  })
})
