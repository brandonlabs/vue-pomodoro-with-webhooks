import Vue from 'vue'
import Router from 'vue-router'
import Pomodoro from '@/components/Pomodoro'
import IntegrationManager from '@/components/IntegrationManager'
import Auth from '@okta/okta-vue'
// import Config from '../Config.json'

Vue.use(Auth, {
  // issuer: Config.auth.okta.issuer,
  // client_id: Config.auth.okta.clientId,
  client_id: "{{ insert_client_id }}",
  issuer: "{{ insert_issuer }}",
  redirect_uri: 'http://localhost:8080/implicit/callback',
  scope: 'openid profile email'
})

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Pomodoro',
      component: Pomodoro
    },
    {
      path: '/implicit/callback',
      component: Auth.handleCallback()
    },
    {
      path: '/integrations',
      name: 'Integrations',
      component: IntegrationManager,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach(Vue.prototype.$auth.authRedirectGuard())

export default router
