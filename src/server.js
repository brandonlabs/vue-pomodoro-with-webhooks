const axios = require('axios')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const epilogue = require('epilogue')
const OktaJwtVerifier = require('@okta/jwt-verifier')
// const Config = require('./Config.json')

const oktaJwtVerifier = new OktaJwtVerifier({
  // clientId: Config.auth.okta.clientId,
  // issuer: Config.auth.okta.issuer
  clientId: "{{ insert_client_id }}",
  issuer: "{{ insert_issuer }}"
})

let app = express()
app.use(cors())
app.use(bodyParser.json())

// verify JWT token middleware
app.use((req, res, next) => {
  // require every request to have an authorization header
  if (!req.headers.authorization) {
    return next(new Error('Authorization header is required'))
  }
  let parts = req.headers.authorization.trim().split(' ')
  let accessToken = parts.pop()
  oktaJwtVerifier.verifyAccessToken(accessToken)
    .then(jwt => {
      req.user = {
        uid: jwt.claims.uid,
        email: jwt.claims.sub
      }
      next()
    })
    .catch(next) // jwt did not verify!
})

// We are going to use SQLite to limit dependencies
let database = new Sequelize({
  dialect: 'sqlite',
  storage: './test.sqlite'
})

// Define our Integration model
// id, createdAt, and updatedAt are added by sequelize automatically
let Integration = database.define('integrations', {
  title: Sequelize.STRING,
  url: Sequelize.STRING,
})

// Initialize epilogue
epilogue.initialize({
  app: app,
  sequelize: database
})

// Create the dynamic REST resource for our Integration model
let userResource = epilogue.resource({
  model: Integration,
  endpoints: ['/integrations', '/integrations/:id']
})

app.post('/trigger/integrations', function (req, res) {
  // Fetch integrations for user and then hit them one by one
  // Currently set to work for Slack - haven't catered for other providers
  Integration.findAll({
    attributes: ['url', 'body']
  }).then((data) => {
    // Data is an array of integrations
    if(data.length){
      data.map( (integration) => {
        axios({
          method: 'post',
          url: integration.dataValues.url,
          data: {
            text: 'Well done!\nTime to take a break!'
          }
        })
      })
    }
  })
})

// launches the express app on :8081
database
  .sync({ force: false })
  .then(() => {
    app.listen(8081, () => {
      console.log('listening to port localhost:8081')
    })
  })