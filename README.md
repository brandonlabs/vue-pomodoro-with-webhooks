# pomodoro

> A Vue.js project

In order to run this app, you will need to create a free OKTA developer account (https://developer.okta.com/signup/). Once creating the account, you will need to create an application and then fill the issuer (url) and client id into the {{ insert_issuer }} and {{ insert_client_id }} placeholders (in src/server.js and src/index.js) - until the Config.json file is working.

The ontegration support is currently optimized for Slack. To add Slack integration, simply create a Slack webhook integration and set the URL in the web app's integration secrtion.

## Start web app

``` bash
# install dependencies
npm install OR yarn install

# serve with hot reload at localhost:8080
npm run dev OR yarn start

```

## Start server

``` bash
# run the server on localhost:8081
node ./src/server

```
